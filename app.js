const url = "https://treinamento-api.herokuapp.com/books"

const parseRequestToJSON = (requestResult) => {
    return requestResult.json();
}

fetch(url)
.then(parseRequestToJSON)
.then((data) => {
     const tabela = document.getElementById("render");
     for(let i=0; i < data.length; i++){
         let index = document.createElement('tr'); //tr é tabela, th cada linha
         index.innerHTML += 
         `
             <th>name: ${data[i].name}</th>
             <th>autor:${data[i].author}</th>
             <th>criado:${data[i].created_at}</th>
             <th>atualizado:${data[i].updated_at}</th>
             <th><button id="delete">deletar</button></th>
         `
        //  const deletar = document.getElementById("delete");
        // deletar.addEventListener("click", (e)=>{
        //     e.preventDefault();

        //     fetch(`${url}/books/${id}`, {
        //         method: 'DELETE'
        //     }).then((resposta) => {
        //         if (resposta.status === 404) alert ('Página não encontrada')
        //         else console.log(resposta);})
        //     .catch( erro => alert(erro))  
        // }) 
        // }

         tabela.appendChild(index);
     }})

const newBook = document.getElementById("new-book");



const parseRequesteToJSON = (requestResult) =>{
    return requestResult.json();  //método json que retorna uma promese, logo precisa de return
}

const myHeaders = {
    "Content-Type": "application/json"
}

newBook.addEventListener("click", (e) => {
    e.preventDefault();

    const name = document.getElementById("name-book").value;
    const author = document.getElementById("author-book").value;

    const novoLivro = {
        "book":{
            name,
            author 
        }
    }
    
    const fetchConfig = {
        method: 'POST',
        headers: myHeaders,
        body: JSON.stringify(novoLivro)
    }

    fetch('https://treinamento-api.herokuapp.com/books', fetchConfig)
    .then(parseRequesteToJSON)
    .then((respondeAsJson) => {
        console.log(respondeAsJson);
    })
})